package com.sistemamf.log.service;

import com.sistemamf.cliente.model.Cliente;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class LogService {
    public void gravarLog(Cliente cliente){
        try {
            File arquivo = new File( "log.csv");
            StringBuilder sb = new StringBuilder();
            if (!arquivo.exists()) {
                arquivo.createNewFile();
                sb.append("Cliente");
                sb.append(',');
                sb.append("Ativo");
                sb.append('\n');
            }
            sb.append(cliente.getCliente());
            sb.append(',');
            sb.append(cliente.isAcesso());
            sb.append('\n');

            FileWriter fw = new FileWriter( arquivo, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(sb.toString());
            bw.close();fw.close();

            System.out.println("Log Salvo!");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.sistemamf.log;

import com.sistemamf.cliente.model.Cliente;
import com.sistemamf.log.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogConsumer {
    @Autowired
    private LogService logService;

    @KafkaListener(topics = "spec2-mariana-afonso-1", groupId = "teste")
    public void receber(@Payload Cliente cliente) {
        System.out.println("O cliente " + cliente.getCliente() + " com acesso a porta ativo " + cliente.isAcesso());
        logService.gravarLog(cliente);
    }
}

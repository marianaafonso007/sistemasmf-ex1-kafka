package com.sistemamf.cliente.model;

public class Cliente {
    private String cliente;
    private String porta;
    private boolean acesso;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        porta = porta;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }
}

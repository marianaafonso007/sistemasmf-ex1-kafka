package com.sistemamf.cliente.controller;

import com.sistemamf.cliente.model.Cliente;
import com.sistemamf.cliente.producer.ClienteProducer;
import com.sistemamf.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteProducer clienteProducer;

    @GetMapping("/{cliente}/{porta}")
    public void verificarAcesso(@PathVariable String cliente, @PathVariable String porta){
        Cliente client = new Cliente();
        client.setCliente(cliente);
        client.setPorta(porta);
        client.setAcesso(clienteService.validarCliente());
        clienteProducer.enviarKafka(client);
    }
}

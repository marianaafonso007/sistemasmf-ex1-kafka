package com.sistemamf.cliente.producer;

import com.sistemamf.cliente.model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {
    @Autowired
    private KafkaTemplate<String, Cliente> producer;

    public void enviarKafka(Cliente cliente){
        producer.send("spec2-mariana-afonso-1", cliente);
    }

}
